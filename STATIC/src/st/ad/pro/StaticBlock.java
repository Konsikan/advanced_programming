package st.ad.pro;

public class StaticBlock {

	public static void main(String args[]) {
		System.out.println("HI FROM MAIN!");
	}

	static {// static block will invoke before main method
		System.out.println("HI FROM STATIC BLOCK!");
	}
}

/*

OUTPUT

HI FROM STATIC BLOCK!
HI FROM MAIN!

*/