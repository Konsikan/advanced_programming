package array.ad.pro;

public class TestArray2 {
	public static void main(String args[]) {
		
		int array[] = new int[5];
		
		array[0] = 80;
		array[1] = 50;
		array[2] = 20;
		array[3] = 90;
		array[4] = 40;

		for (int i = 0; i < array.length; i++) {
			
			System.out.println(array[i]);
		
		}
	}
}

/*

OUTPUT

10
20
70
40
50

*/