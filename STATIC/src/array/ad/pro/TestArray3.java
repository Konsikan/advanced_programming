package array.ad.pro;

public class TestArray3 {

	public static void main(String args[]) {

		int Siva[] = new int[5];// count of subjects

		Siva[0] = 70;
		Siva[1] = 75;
		Siva[2] = 80;
		Siva[3] = 90;
		Siva[4] = 65;

		int total = 0;

		for (int i = 0; i < Siva.length; i++) {
			total = total + Siva[i]; // find the total marks
		}

		int average = total / Siva.length;// average of the total marks

		System.out.println("TOTAL : " + total);
		System.out.println("AVERAGE : " + average);
	}
}

/*
 * 
 * OUTPUT
 * 
 * TOTAL : 380 AVERAGE : 76
 * 
 */