package array.ad.pro;

import java.util.Scanner;

public class TestArray3UsingUserInput {

	public static void main(String args[]) {

		int total = 0;

		Scanner input1 = new Scanner(System.in);

		System.out.print("NUMBER OF SUBJECTS : ");
		int input = input1.nextInt(); // count of subjects
		System.out.println();

		int siva[] = new int[input];
		for (int i = 0; i < input; i++) {
			System.out.print("SUBJECT " + (i + 1) + " : ");
			int marks = input1.nextInt(); // marks input
			siva[i] = marks;
		}

		for (int i = 0; i < siva.length; i++) {
			total = total + siva[i]; // find the total marks
		}

		double average = total / siva.length; // average of the total marks

		System.out.println();
		System.out.println("TOTAL \t\t: " + total);
		System.out.println("AVERAGE \t: " + average);
	}
}

/*

OUTPUT

NUMBER OF SUBJECTS : 5

SUBJECT 1 : 14
SUBJECT 2 : 75
SUBJECT 3 : 86
SUBJECT 4 : 92
SUBJECT 5 : 78

TOTAL 		: 345
AVERAGE 	: 69.0

 */
