package array.ad.pro;

import java.util.Scanner;

public class MarksUsingArrayMethodCall {
	static int total = 0;
	static double average = 0;

	private static void total(int marks[]) { // find the total marks
		
		for (int i = 0; i < marks.length; i++) {
			total = total + marks[i]; 
		}
		System.out.println("TOTAL \t\t: " + total);
		
	}

	private static void average(int size) { // average of the total marks
		
		double average = total / size; 
		System.out.println("AVERAGE \t: " + average);
		
	}

	private static void displayMarksInput(int marks[]) { // display marks
		System.out.print("YOUR MARKS : ");
		for (int i = 0; i < marks.length; i++) {
			System.out.print("| "+marks[i]+" ");
		}
		
	}

	public static void main(String args[]) {

		Scanner input1 = new Scanner(System.in);

		System.out.print("NUMBER OF SUBJECTS : ");
		int input = input1.nextInt(); // Number of subjects
		System.out.println();
		int marks[] = new int[input];
		
		for (int i = 0; i < input; i++) {
			System.out.print("SUBJECT " + (i + 1) + " : ");
			int markInput = input1.nextInt(); // marks input
			marks[i] = markInput;
		}
		
		System.out.println();
		displayMarksInput(marks);
		System.out.println();
		System.out.println();
		total(marks);
		average(marks.length);

	}
}

/*

OUTPUT

NUMBER OF SUBJECTS : 5

SUBJECT 1 : 48
SUBJECT 2 : 78
SUBJECT 3 : 95
SUBJECT 4 : 47
SUBJECT 5 : 52

YOUR MARKS : | 48 | 78 | 95 | 47 | 52  

TOTAL 		: 320
AVERAGE 	: 64.0

*/
