package electricityBill;

import java.util.Scanner;

/**
 * LogIn or SignIn data are came from here.
 * 
 * @author J.Konsikan
 * @version 1.0
 *
 */
public class LogInSignIn {
	/**
	 * @param username import user name..
	 * @param password import password..
	 */
	public void logIn(String username, String password) {
		PrintingMessages message = new PrintingMessages();
		message.logInOrSignIn(2);
		int correction = 1;
		while (correction == 1) {
			System.out.println();
			Scanner input = new Scanner(System.in);
			System.out.print("USER NAME\t: ");
			String inputusername = input.next();
			System.out.print("PASSWORD\t: ");
			String inputpassword = input.next();
			if (inputusername.equals(username) && inputpassword.equals(password)) {
				message.logInOrSignIn(4);
				break;
			} else {
				correction = 1;
				message.logInOrSignIn(5);
			}
		}
	}

	public void signIn() {
		PrintingMessages message = new PrintingMessages();
		message.logInOrSignIn(3);
		Scanner input = new Scanner(System.in);
		System.out.print("USER NAME\t: ");
		String username = input.next();
		System.out.print("PASSWORD\t: ");
		String password = input.next();
		message.logInOrSignIn(6);
		LogInSignIn main = new LogInSignIn();
		main.logIn(username, password);
	}
}
