package electricityBill;

import java.util.Scanner;

/**
 * Customer Reports & Details and calculations are went from here.
 * 
 * @author J.Konsikan
 * @version 1.0
 *
 */
public class CustomerDetails {

	/**
	 * new Customers added by admin from here.
	 * 
	 */
	public void addNewDetails() {
		PrintingMessages message = new PrintingMessages();
		Scanner input = new Scanner(System.in);
		message.CustomerDetailsAddNewDetails(1);
		// HOW MANY CUSOMERS NEED TO ADD
		int numberOfUsers = input.nextInt();
		System.out.println();
		CustomerDetails select = new CustomerDetails();
		int[] userID = new int[numberOfUsers];
		String[] userName = new String[numberOfUsers];
		String[] address = new String[numberOfUsers];
		int[] units = new int[numberOfUsers];
		for (int i = 0; i < numberOfUsers; i++) {
			System.out.print("USER ID \t:");
			Scanner input1 = new Scanner(System.in);
			userID[i] = input1.nextInt();
			Scanner input2 = new Scanner(System.in);
			System.out.print("USER NAME \t:");
			userName[i] = input2.nextLine();
			Scanner input3 = new Scanner(System.in);
			System.out.print("ADDRESS \t:");
			address[i] = input3.nextLine();
			Scanner input5 = new Scanner(System.in);
			System.out.print("UNITS \t\t:");
			units[i] = input3.nextInt();
			System.out.println();
		}
		message.CustomerDetailsAddNewDetails(2);
		// NOW YOU CAN GET THE REPORT OF THE CUSTOMERS
		select.viewDetailsForAdmin(userID, userName, address, units);
	}

	/**
	 * @param type form this parameter we can select the type of who can access
	 *   admin or user?
	 */
	public void oldCustomerDetails(int type) {
		CustomerDetails select = new CustomerDetails();
		int[] userID = { 1001, 1002, 1003, 1004, 1005 };
		String[] userName = { "JKONSIKAN", "SSILAXAN", "DILUXIHA", "PRESTINA", "PRASHA" };
		String[] address = { "PointPedro", "Jaffna", "Nelliady", "Achchuvely", "Kilinochi" };
		int[] units = { 41, 72, 89, 92, 98 };
		if (type == 1) {
			select.viewDetailsForAdmin(userID, userName, address, units);
		}
		if (type == 2) {
			select.viewDetailsForCustomer(userID, userName, address, units);
		}
	}
 
	/**
	 * Admin can access this section.
	 * 
	 * @param userID
	 * @param userName
	 * @param address
	 * @param units
	 */
	public void viewDetailsForAdmin(int[] userID, String[] userName, String[] address, int[] units) {
		CustomerDetails select = new CustomerDetails();
		PrintingMessages message = new PrintingMessages();
		message.CustomerDetailsViewDetailsForAdminAndTeacher();
		// YOU CAN SEE THE Customer DETAILS HERE
		// 1-SPECIFIC CUSTOMER REPORT | 2-ALL CUSTOMER REPORTS
		// 4-HIGHER UNITS HOLDER REPORT
		int correction = 1;
		while (correction == 1) {
			message.type();
			Scanner input = new Scanner(System.in);
			int type = input.nextInt();
			System.out.println();
			if (type == 1) {
				select.specificCustomerDetails(userID, userName, address, units);
				break;
			} else if (type == 2) {
				select.allCustomerDetails(userID, userName, address, units);
				break;
			} else if (type == 3) {
				select.higherUnitsHolder(userID, userName, address, units);
				break;
			} else {
				correction = 1;
				message.incorrectValue();
			}
		}
	}

	/**
	 * Customers can access this section.
	 * 
	 * @param userID
	 * @param userName
	 * @param address
	 * @param units
	 */

	public void viewDetailsForCustomer(int[] userID, String[] userName, String[] address, int[] units) {
		CustomerDetails select = new CustomerDetails();
		PrintingMessages message = new PrintingMessages();
		select.specificCustomerDetails(userID, userName, address, units);
	}
	
	/**
	 * In this section all Customer details going to be in a ranking order.
	 * 
	 */
	public void rankedList(int[] userID, String[] userName, String[] address, int[] units,int typ,int uINo) {
		PrintingMessages message = new PrintingMessages();
		CustomerDetails select = new CustomerDetails();
		int m[] = new int[userID.length];
		int temp;
		int uID;
		String uName;
		String uAddress;
		int uUnits;
		for (int i = 0; i < userID.length; i++) {
			int max = units[i];
			m[i] = max;
		}
		for (int j = 0; j < userID.length; j++) {
			for (int k = j + 1; k < userID.length; k++) {
				if (m[j] < m[k]) {
					temp = m[j];
					m[j] = m[k];
					m[k] = temp;

					uID = userID[j];
					userID[j] = userID[k];
					userID[k] = uID;

					uName = userName[j];
					userName[j] = userName[k];
					userName[k] = uName;

					uAddress = address[j];
					address[j] = address[k];
					address[k] = uAddress;

					uUnits = units[j];
					units[j] = units[k];
					units[k] = uUnits;
				}
			}
		}
		if (typ == 1) {
			// all customer reports
			for (int n = 0; n < userID.length; n++) {
				if (n > 0) {
					if (m[n] == m[n - 1]) {
						System.out.println("\t\tBILL NO \t:" + n);
						select.getDetails(userID[n], userName[n], address[n], units[n]);
					}
				}
				if (n > 0) {
					if (m[n] != m[n - 1]) {
						System.out.println("\t\tBILL NO \t:" + n);
						select.getDetails(userID[n], userName[n], address[n], units[n]);
					}
				}

			}
		}
		if (typ == 2) {
			// specific customer report
			for (int i = 0; i < userID.length; i++) {
				int uSNo = userID[i];
				if (uINo == uSNo) {
					System.out.println("\t\tBILL NO \t:" + i);
					select.getDetails(userID[i], userName[i], address[i], units[i]);
					break;
				} else {
					if (i == userID.length - 1) {
						message.incorrectValue();
					}
				}
			}
		}

	}



	public void allCustomerDetails(int[] userID, String[] userName, String[] address, int[] units) {
		PrintingMessages message = new PrintingMessages();
		CustomerDetails select = new CustomerDetails();
		message.CustomerDetailsOthers(1);
		// YOU CAN SEE ALL CUSTOMER REPORTS HERE
		select.rankedList(userID, userName, address, units,1,0);
	}

	public void specificCustomerDetails(int[] userID, String[] userName, String[] address, int[] units) {
		CustomerDetails select = new CustomerDetails();
		PrintingMessages message = new PrintingMessages();
		message.CustomerDetailsOthers(2);
		int correction1 = 1;
		while (correction1 == 1) {
			System.out.println();
			Scanner input = new Scanner(System.in);
			System.out.print("COSTOMER ID : ");
			int uINo = input.nextInt();
			System.out.println();
			select.rankedList(userID, userName, address, units,2,uINo);
		}
	}

	/**
	 * Higher units holder details from here.
	
	 * @param userID
	 * @param userName
	 * @param address
	 * @param units
	 */
	public void higherUnitsHolder(int[] userID, String[] userName, String[] address, int[] units) {
		PrintingMessages message = new PrintingMessages();
		CustomerDetails grad = new CustomerDetails();
		int maxUnits = 0;
		int maxUserID = 0;
		String maxUserName = null;
		String maxAddress = null;
		for (int i = 0; i < units.length; i++) {
			if (units[i] > maxUnits) {
				maxUnits = units[i];
				maxUserID = userID[i];
				maxUserName = userName[i];
				maxAddress = address[i];
			}
		}
		System.out.println("\t\t\t    **********************************");
		System.out.println("\t\t\t       TOP UNITS USER  " + userName);
		System.out.println("\t\t\t      ===============================");
		System.out.println("\t\t\t         USER ID \t: " + maxUserID);
		System.out.println("\t\t\t         NAME \t: " + maxUserName);
		System.out.println("\t\t\t         ADDRESS \t: " + address);
		System.out.println("\t\t\t         UNITS \t: " + maxUnits);
		grad.billingAmount(maxUnits);
		System.out.println("\t\t\t    **********************************");
	}

	/**
	 * Customer reports made from here.
	 * 
	 * @param userID
	 * @param userName
	 * @param address
	 * @param units
	 */
	public void getDetails(int userID, String userName, String address, int units) {
		CustomerDetails grad = new CustomerDetails();
		System.out.println();
		System.out.println("\t     *****************************************************************");
		System.out.println("\t\tUSER ID \t:" + userID);
		System.out.println("\t\tUSER NAME \t:" + userName);
		System.out.println("\t\tADDRESS \t:" + address);
		System.out.println("\t     -----------------------------------------------------------------");
		System.out.println();
		System.out.println("\t\tUNITS \t\t:" + units);
		grad.billingAmount(units);
		System.out.println("\t     =================================================================");
		System.out.println();
		System.out.println();
	}

	/**
	 * unit calculating system.
	 * 
	 * @param units
	 */
	public void billingAmount(int units) {
		int amount=0;
		if (units > 120) {
			amount = 20*units;
		}
		if ((units <= 120) && (units >= 90)) {
			amount = 15*units;
		}
		if ((units < 90) && (units > 0)) {
			amount = 10*units;
		}
		System.out.println("\t\tAMOUNT \t\t:" + amount);
	}
}
