package electricityBill;

/**
 * All printing messages are went from here.
 * 
 * @author J.Konsikan
 * @version 1.0
 *
 */
public class PrintingMessages {
	/**
	 * @param select this is for easy calling method..
	 */
	public void start(int select) {

		if (select == 1) {
			System.out.println();
			System.out.println("\t\t           | SIMPLE ELECTRICITY BILLING SYSTEM |");
			System.out.println();
		}
		if (select == 2) {
			System.out.println();
			System.out.println("\t\t\t\t    | 1-ADMIN | 2-USER |");
			System.out.println("\t\t\t\t  ========================");
		}
	}

	/**
	 * @param select this is for easy calling method..
	 */
	public void panels(int select) {
		if (select == 1) {
			System.out.println("\t\t\t\t       *************");
			System.out.println("\t\t\t\t        ADMIN PANEL");
			System.out.println("\t\t\t\t       *************");
		}
		if (select == 2) {
			System.out.println("\t\t\t\t       *************");
			System.out.println("\t\t\t\t         USER PANEL");
			System.out.println("\t\t\t\t       *************");
		}
	}

	/**
	 * @param select this is for easy calling method..
	 */
	public void adminPanel(int select) {

		if (select == 1) {
			System.out.println("\t\t\t ******************************************");
			System.out.println("\t\t\t     YOU NEED ADMIN PERMISSION TO CREAT");
			System.out.println("\t\t\t ******************************************");
		}
		if (select == 2) {
			System.out.println("\t\t\t     ********************************");
			System.out.println("\t\t\t           $$ ACCESS GRANTED $$");
			System.out.println("\t\t\t     ********************************");
		}
	}

	public void userPanel() {
		System.out.println("\t\t\t******************************************");
		System.out.println("\t\t\t  YOU CAN SEE ONLY SPECIFIC DETAILS HERE");
		System.out.println("\t\t\t******************************************");
	}

	/**
	 * @param select this is for easy calling method..
	 */
	public void logInOrSignIn(int select) {
		if (select == 1) {
			System.out.println();
			System.out.println("\t\t\t     =================================");
			System.out.println("\t\t\t     | 1-LOGIN | 2-CREAT NEW ACCOUNT |");
			System.out.println("\t\t\t     =================================");
		}
		if (select == 2) {
			System.out.println("\t\t\t\t\t  =======");
			System.out.println("\t\t\t\t\t  [LOGIN]");
			System.out.println("\t\t\t\t\t  =======");
		}
		if (select == 3) {
			System.out.println("\t\t\t\t      ================");
			System.out.println("\t\t\t\t      [CREATE ACCOUNT]");
			System.out.println("\t\t\t\t      ================");
		}
		if (select == 4) {
			System.out.println();
			System.out.println("\t\t\t\t| $ YOU ARE LOGGED IN $ |");
			System.out.println();
		}
		if (select == 5) {
			System.out.println();
			System.out.println("\t\t\t  | !! INCORRECT USER NAME & PASSWORD !! |");
			System.out.println("\t\t\t              PLEASE TRY AGAIN");
		}
		if (select == 6) {
			System.out.println();
			System.out.println("\t\t\t$$ YOUR ACCOUNT WAS SUCCESSFULLY CREATED $$");
			System.out.println();
		}
	}

	public void type() {
		System.out.println();
		System.out.println("\t\t\t\t\t   TYPE!");
		System.out.print("\t\t\t\t\t     ");
	}

	public void incorrectValue() {
		System.out.println("\t\t\t\t   !! INCORRECT VALUE !!");
		System.out.println("\t\t\t\t        -TRY AGAIN-  ");
	}

	public void CustomerDetailsMain() {
		System.out.println();
		System.out.println("\t\t    ***************************************************");
		System.out.println("\t\t\t  YOU CAN SEE|ADD THE CUSTOMER DETAILS HERE");
		System.out.println("\t\t    ***************************************************");
		System.out.println();
		System.out.println("\t    1-ADD NEW CUSTOMER DETAILS | 2-VIEW PREVIOUS CUSTOMER DETAILS");

	}

	/**
	 * @param select this is for easy calling method..
	 */
	public void CustomerDetailsAddNewDetails(int select) {
		if (select == 1) {
			System.out.println();
			System.out.println("\t\t\t     : HOW MANY CUSTOMERS NEED TO ADD : ");
			System.out.print("\t\t\t\t\t     ");
		}
		if (select == 2) {
			System.out.println("\t\t   ****************************************************");
			System.out.println("\t\t\t NOW YOU CAN GET THE REPORT OF THE CUSTOMERS");
			System.out.println("\t\t   ****************************************************");
		}

	}

	public void CustomerDetailsViewDetailsForAdminAndTeacher() {
		System.out.println("\t\t\t******************************************");
		System.out.println("\t\t\t   YOU CAN SEE THE CUSTOMER DETAILS HERE");
		System.out.println("\t\t\t******************************************");
		System.out.println("");
		System.out.println("\t\t     1-SPECIFIC CUSTOMER BILL  | 2-ALL CUSTOMER's BILLS ");
		System.out.println("\t\t\t  3-HIGHER UNITS HOLDER DETAILS ");
	}

	/**
	 * @param select this is for easy calling method..
	 */
	public void CustomerDetailsOthers(int select) {
		if (select == 1) {
			System.out.println("\t\t\t******************************************");
			System.out.println("\t\t\t   YOU CAN SEE ALL CUSTOMER BILLS HERE");
			System.out.println("\t\t\t******************************************");
			System.out.println();
		}
		if (select == 2) {
			System.out.println();
			System.out.println("\t\t\t*******************************************");
			System.out.println("\t\t         YOU CAN SEE THE SPECIFIC CUSTOMER BILL HERE!");
			System.out.println("\t\t\t*******************************************");
		}
		if (select == 3) {
			System.out.println("\t\t\t*******************************************");
			System.out.println("\t\t  YOU CAN SEE HIGHER UNITS HOLDER DETAILS HERE");
			System.out.println("\t\t\t*******************************************");
		}
	}

}
