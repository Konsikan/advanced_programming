package electricityBill;

import java.util.Scanner;

/**
 * 
 * Main system start from here.
 * 
 * @author J.Konsikan
 * @version 1.0
 * @since 22.06.2020
 *
 */
public class Main {

	public static void main(String args[]) {
		PrintingMessages message = new PrintingMessages();
		CustomerDetails calling = new CustomerDetails();
		LogInSignIn loginOrSignIn = new LogInSignIn();
		Main main =new Main();
		message.start(1);
		// SIMPLE ELECTRICITY BILLING SYSTEM
		message.start(2);
		// 1-ADMIN | 2-USER |
		int correction1 = 1;
		while (correction1 == 1) {
			Scanner input1 = new Scanner(System.in);
			message.type();
			int type = input1.nextInt();
			System.out.println();
			if (type == 1) {
				String username = "ADMIN01";
				String password = "ADMINBCAS01";
				loginOrSignIn.logIn(username, password);
				main.adminWork();
				break;
			} else if (type == 2) {
				
				message.logInOrSignIn(1);
				// 1-LOGIN | 2-CREAT NEW ACCOUNT
				Scanner input = new Scanner(System.in);
				int correction = 1;
				while (correction == 1) {
					message.type();
					int type1 = input.nextInt();
					System.out.println();
					if (type1 == 1) {
						String username = "USER01";
						String password = "USERBCAS01";
						loginOrSignIn.logIn(username, password);
						break;
					} else if (type1 == 2) {
						message.adminPanel(1);
						// YOU NEED ADMIN PERMISION TO CREAT!
						String username = "ADMIN01";
						String password = "ADMINBCAS01";
						loginOrSignIn.logIn(username, password);
						message.adminPanel(2);
						// ACCESS GRANDED
						loginOrSignIn.signIn();
						break;
					} else {
						correction = 1;
						message.incorrectValue();
					}
					
				}
				calling.oldCustomerDetails(2);
				break;
			} else {
				correction1 = 1;
				message.incorrectValue();
			}
		}
	}
	
	/**
	 * @param grade form this parameter all of sections get which grade is?..
	 */
	public void adminWork() {
		CustomerDetails calling = new CustomerDetails();
		PrintingMessages message = new PrintingMessages();
		message.CustomerDetailsMain();
		// YOU CAN SEE|ADD THE CUSTOMER DETAILS HERE
		// ADD NEW CUSTOMER DETAILS | 2-VIEW PREVIOUS CUSTOMER DETAILS
		int correction = 1;
		while (correction == 1) {
			Scanner input = new Scanner(System.in);
			message.type();
			int type = input.nextInt();
			System.out.println();
			if (type == 1) {
				calling.addNewDetails();
				break;
			} else if (type == 2) {
				calling.oldCustomerDetails(1);
				break;
			} else {
				correction = 1;
				message.incorrectValue();
			}
		}
	}
}
