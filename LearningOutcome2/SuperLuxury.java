package LO2;

public class SuperLuxury extends LuxuryRoom {
	
	//Private room's square Feet
	private double squareFeet;
	
	public SuperLuxury(double baseFee,int numberOfDays) {
		super(baseFee, numberOfDays);
	}
	
	@Override
	double getCost() {
		return  (squareFeet * getBaseFee())+ getRentalFee();
	}	
	
	//accessor method
	public double getSquareFeet() {
		return squareFeet;
	}
	
	public void setSquareFeet(double squareFeet) {
		this.squareFeet = squareFeet;
	}
}
