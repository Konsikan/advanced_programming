package LO2;

public class SemiLuxury extends LuxuryRoom{
	
	//Private number of beds
	private int nbBeds;
	
	public SemiLuxury(double baseFee,int nbBeds) {
		super( baseFee, nbBeds);
	}
	
	@Override
	double getCost() {
		return (nbBeds * getBaseFee())+ getRentalFee();
	}
	
	//accessor method
	public int getNbBeds() {
		return nbBeds;
	}
	
	public void setNbBeds(int nbBeds) {
		this.nbBeds = nbBeds;
	}
}

