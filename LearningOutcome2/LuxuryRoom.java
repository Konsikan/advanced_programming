package LO2;

public class LuxuryRoom extends RentedRoom {

	private int numberOfDays;
	
	public LuxuryRoom (double baseFee, int numberOfDays) {
		super(baseFee);
			this.numberOfDays = numberOfDays;
		}
	
	public double getRentalFee() {
		if (numberOfDays< 5) {
			return 100 * getNumberOfDays();
			
		} else if(5<=numberOfDays && numberOfDays <=10) {
			return 80 * getNumberOfDays();
			
		}
			return 50 * getNumberOfDays();
	}
	
	//accessor method
	public int getNumberOfDays() {
		return numberOfDays;
		}

	public void setNumberOfDays(int numberOfDays) {
		this.numberOfDays = numberOfDays;
		}
}
