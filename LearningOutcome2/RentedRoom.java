package LO2;

public  class RentedRoom {
	
	private double baseFee;
	
	public RentedRoom(double baseFee){
		this.baseFee = baseFee;
	}

	double getCost() {
		return baseFee;
	}
	
	//accessor method
	public double getBaseFee() {
		return baseFee;
	}
	
	public void setBaseFee(double baseFee) {
		this.baseFee = baseFee;
	}
}

