package LO2;

public class NormalRoom extends RentedRoom {

	//private integer number of days
	private int nbDays;

	public NormalRoom(double baseFee, int nbDays) {
		super(baseFee);
		this.nbDays= nbDays;
	}

	@Override
	double getCost() {
		return getBaseFee() * nbDays;	
	}
	
	public int getNbDays() {
		return nbDays;
	}
	
	public void setNbDays(int nbDays) {
		this.nbDays = nbDays;
	}
}
