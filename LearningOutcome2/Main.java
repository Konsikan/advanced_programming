package LO2;

import java.util.Scanner;

public class Main {

		public static void main(String[] args) {
		
		try (Scanner input = new Scanner(System.in)) {
			System.out.println("\n\n\t\t\t\t\t^^^^^^^^^^^^^^^^^^^ | WELCOME TO DILUX ROOM RENTED BULINDING | ^^^^^^^^^^^^^^^^^^^\n\n");
			
			String[] roomType = {"\t\t\t\t\t============================     NORMAL ROOM 1    ===============================", "\t\t\t\t\t============================   SEMILUXURY ROOM 2  ===============================", "\t\t\t\t\t============================   SUPERLUXURY ROOM 3 ==============================="};
			
			for (int i=0; i<roomType.length; i++) {
				System.out.println(roomType[i]);
			}
			
			System.out.println("\n\t\t\t\t\t\t\t\t | Enter Which Room do you want !! | \n");
			int inputNo = input.nextInt();
			
			switch (inputNo){
			case 1:
				NormalRoom obj1 = new NormalRoom(1000, 2);
				obj1.getCost();
				System.out.println("\t\t\t\t\t\t\t*******************************************");
				System.out.println("\t\t\t\t\t\t\t\t\t NORMALROOM");
				System.out.println("\t\t\t\t\t\t\t*******************************************");
				System.out.println("\t\t\t\t\t\t\t*******************************************");
				System.out.println("\t\t\t\t\t\t\t\t\t   "+obj1.getNbDays()+" DAYS");
				System.out.println("\t\t\t\t\t\t\t*******************************************");
				System.out.println("\t\t\t\t\t\t\t*******************************************");
				System.out.println("\t\t\t\t\t\t\t\t\tONLY "+obj1.getCost());
				System.out.println("\t\t\t\t\t\t\t*******************************************");
				break;
			case 2:
				SemiLuxury obj2 = new SemiLuxury(5000, 4);
				obj2.setNbBeds(2);
				System.out.println("\t\t\t\t\t\t\t*******************************************");
				System.out.println("\t\t\t\t\t\t\t\t      SEMILUXURY ROOM");
				System.out.println("\t\t\t\t\t\t\t*******************************************");
				System.out.println("\t\t\t\t\t\t\t*******************************************");
				System.out.println("\t\t\t\t\t\t\t\t\t   "+obj2.getNbBeds()+" BEDS");
				System.out.println("\t\t\t\t\t\t\t*******************************************");
				System.out.println("\t\t\t\t\t\t\t*******************************************");
				System.out.println("\t\t\t\t\t\t\t\t\tONLY "+obj2.getCost());
				System.out.println("\t\t\t\t\t\t\t*******************************************");
				break;
			case 3:
				SuperLuxury obj3 = new SuperLuxury(3000, 3);
				obj3.setSquareFeet(6.3);
				System.out.println("\t\t\t\t\t\t\t*******************************************");
				System.out.println("\t\t\t\t\t\t\t\t      SUPERLUXURY ROOM");
				System.out.println("\t\t\t\t\t\t\t*******************************************");
				System.out.println("\t\t\t\t\t\t\t*******************************************");
				System.out.println("\t\t\t\t\t\t\t\t      "+obj3.getSquareFeet()+" SQUARE FEET");
				System.out.println("\t\t\t\t\t\t\t*******************************************");
				System.out.println("\t\t\t\t\t\t\t*******************************************");
				System.out.println("\t\t\t\t\t\t\t\t\tONLY "+obj3.getCost());
				System.out.println("\t\t\t\t\t\t\t*******************************************");
				break;
			default :
				System.out.println("\t\t\t\t\t\t\t*******************************************");
				System.out.println("\t\t\t\t\t\t\t\t !!!!! INCORRECT VALUE !!!!!");
				System.out.println("\t\t\t\t\t\t\t\t        - TRY AGAIN -");
				System.out.println("\t\t\t\t\t\t\t*******************************************");
			}
		}	
	}	
}

/*
 				IF WE TYPE 1 
  				OUTPUT LOOK LIKE :
  				*******************************************
								NORMALROOM
				*******************************************
				*******************************************
								  2 DAYS
				*******************************************
				*******************************************
								ONLY 2000.0
				*******************************************
 */

/*
				IF WE TYPE 2 
				OUTPUT LOOK LIKE :
				*******************************************
							 SEMILUXURY ROOM
				*******************************************
				*******************************************
								 2 BEDS
				*******************************************
				*******************************************
							  ONLY 10400.0
				*******************************************
*/

/*
				IF WE TYPE 2 
				OUTPUT LOOK LIKE :
 				*******************************************
							 SUPERLUXURY ROOM
				*******************************************
				*******************************************
						      6.3 SQUARE FEET
				*******************************************
				*******************************************
							   ONLY 19200.0
				*******************************************
*/
